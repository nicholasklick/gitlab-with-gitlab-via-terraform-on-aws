data "aws_ami" "GitLab-EE" {
  most_recent = true

  filter {
    name   = "name"
    values = ["GitLab EE ${var.gitlab_version}"]
  }
  #
  # name_regex = "^ami-\\d{3}"
  owners = ["782774275127"]
}

resource "aws_instance" "GitLabEE" {
  ami = "${data.aws_ami.GitLab-EE.id}"
  instance_type = "${var.GitLabEE_EC2_instance_type}"
  vpc_security_group_ids = [
    "${aws_security_group.HTTP_HTTPS_SSH.id}",
    "${aws_security_group.Allow_ICMP_GL.id}"
  ]
  key_name = var.ssh_key
  tags = {
    Name = "GitLab_EE-${random_string.zone.result}"
  }

}
