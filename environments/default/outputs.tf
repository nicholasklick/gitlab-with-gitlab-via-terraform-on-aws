output "GitLab-URL" {
  value = "${var.GL_Host_Name}${random_string.zone.result}.${var.GL_Domain}"
}

output "Public-IP-Address-GitLabEE" {
  value = "${aws_instance.GitLabEE.public_ip}"
}

output "Private-IP-Address-GitLabEE" {
  value = "${aws_instance.GitLabEE.private_ip}"
}
